-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3304
-- Generation Time: Jul 26, 2021 at 05:52 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `member_registration_relex`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE `activity` (
  `act_id` int(100) NOT NULL,
  `act_name` varchar(100) NOT NULL,
  `act_date` date NOT NULL,
  `act_time` time NOT NULL,
  `act_location` varchar(100) NOT NULL,
  `act_std_qty` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity`
--

INSERT INTO `activity` (`act_id`, `act_name`, `act_date`, `act_time`, `act_location`, `act_std_qty`) VALUES
(1, 'Kayak', '2021-03-13', '17:00:00', 'Kuala Ibai', 15),
(2, 'Jogging', '2021-03-19', '17:00:00', 'Pantai Pandak', 20),
(3, 'Hiking', '2021-05-28', '17:00:00', 'Bukit Besar', 30),
(4, 'Kayak', '2021-05-06', '17:30:00', 'Kuala Ibai', 15),
(5, 'Jogging', '2021-06-11', '17:00:00', 'Pantai Pandak', 20);

-- --------------------------------------------------------

--
-- Table structure for table `activity_req`
--

CREATE TABLE `activity_req` (
  `req_id` int(100) NOT NULL,
  `req_name` varchar(255) NOT NULL,
  `matric_id` int(20) NOT NULL,
  `act_id` int(100) NOT NULL,
  `act_name` varchar(255) NOT NULL,
  `act_date` date NOT NULL,
  `act_status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_req`
--

INSERT INTO `activity_req` (`req_id`, `req_name`, `matric_id`, `act_id`, `act_name`, `act_date`, `act_status`) VALUES
(1, 'Alia Hairi', 2016282907, 3, 'Hiking', '2021-05-28', 'Accepted'),
(2, 'Siti Hajar', 2018907865, 5, 'Jogging', '2021-06-11', 'Accepted'),
(3, 'Sofia Maisarah', 2016283838, 4, 'Kayak', '2021-05-06', 'Rejected'),
(4, 'Sofia Maisarah', 2016283838, 5, 'Jogging', '2021-06-11', 'Rejected'),
(5, 'Sofia Maisarah', 2016283838, 1, 'Kayak', '2021-03-13', 'Rejected');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(5) NOT NULL,
  `ad_username` varchar(100) NOT NULL,
  `ad_matricno` int(20) NOT NULL,
  `ad_phoneno` int(20) NOT NULL,
  `ad_course` varchar(20) NOT NULL,
  `ad_part` varchar(20) NOT NULL,
  `ad_position` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `ad_username`, `ad_matricno`, `ad_phoneno`, `ad_course`, `ad_part`, `ad_position`) VALUES
(1, 'Ikha', 2019416116, 1123798458, 'CS264', 'Part 4', 'AJK');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `user_id` int(5) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `matric_id` varchar(20) NOT NULL,
  `phone_num` varchar(20) NOT NULL,
  `program` varchar(20) NOT NULL,
  `course` varchar(20) NOT NULL,
  `semester` varchar(20) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `stud_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`user_id`, `full_name`, `matric_id`, `phone_num`, `program`, `course`, `semester`, `gender`, `email`, `stud_status`) VALUES
(1, 'Sofia Maisarah', '2016283838', '00192684265', 'DEG', 'CS264', 'Part 3', 'Female', 'sofea90@gmail.com', 'Accepted'),
(2, 'Alia Hairi', '2016282907', '0192664565', 'DEG', 'CS244', 'Part 6', 'Female', 'hairialia@gmail.com', 'Accepted'),
(3, 'Siti Hajar', '2018907865', '01123567805', 'DEG', 'CS230', 'Part 5', 'Female', 'sitihajjarr@gmail.com', 'Accepted'),
(4, 'Wan Amirul Hakim', '2019083838', '0178907789', 'DIP', 'CS110', 'Part 4', 'Male', 'hakimirul99@gmail.com', 'Accepted'),
(5, 'Alif Syukry', '2019780078', '0135908766', 'DIP', 'CS110', 'Part 4', 'Male', 'alipp@gmail.com', 'Accepted'),
(6, 'Ikhwan Haris', '2020879760', '0178906543', 'DEG', 'CS264', 'Part 2', 'Male', 'harikhwan09@gmail.com', 'Accepted'),
(10, 'Nur Balqis Binti Fadlyirwan', '2019416064', '01117732090', 'DEG', 'CS264', 'Part 4', 'Female', 'nurbalqisfadlyirwan@gmail.com', 'Accepted'),
(11, 'Muhammad Ali', '2018223184', '0177719254', 'DIP', 'CS244', 'Part 1', 'Male', 'ali@gmail.com', 'Accepted'),
(12, 'Amin Ghazali', '2019218337', '0177719254', 'DEG', 'CS244', 'Part 3', 'Male', 'amin@gmail.com', 'Accepted'),
(22, 'Nurul Aina', '2018514215', '0172043576', 'DEG', 'CS264', 'Part 4', 'Female', 'aina00@gmail.com', 'Accepted'),
(23, 'Muhammad Izzat', '2018331672', '0102184423', 'DIP', 'CS110', 'Part 5', 'Male', 'm.izzat@gmail.com', 'Rejected');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`act_id`);

--
-- Indexes for table `activity_req`
--
ALTER TABLE `activity_req`
  ADD PRIMARY KEY (`req_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_req`
--
ALTER TABLE `activity_req`
  MODIFY `req_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `user_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
