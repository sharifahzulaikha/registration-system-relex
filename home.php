<?php 
session_start();

	include("connection.php");
  include("function.php");

	$user_data = check_login($conn);

?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RELEX</title>
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #00CED1;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #04AA6D;
  color: white;
}

.bg-text {
  text-align: center;
}
</style>
</head>
<body>

<div class="topnav">
  <a class="active" href="home.php">Home</a>
  <a href="profile.php">Profile</a>
  <a href="activity.php">Activities</a>
  <a href="logout.php">Log Out</a>
</div>

<div class="bg-text">
	<img src="relexlogo.jpg" width="250" height="200" title="relex logo";>
  	<h1>Welcome to RELEX Club!</h1>
</div>

</head>
<head>
<style>
* {
  box-sizing: border-box;
}

.column {
  float: left;
  width: 33.33%;
  padding: 5px;
}

/* Clearfix (clear floats) */
.row::after {
  content: "";
  clear: both;
  display: table;
}
</style>
</head>
<body>

<p><b>Activities for RELEX Club:</b></p>

<div class="row">
  <div class="column">
    <img src="relex.jpg" alt="Snow" style="width:100%">
  </div>
  <div class="column">
    <img src="kayak.jpg" alt="Forest" style="width:100%">
  </div>
  <div class="column">
    <img src="hiking.jpg" alt="Mountains" style="width:100%">
  </div>
</div>
</body>
</html>
