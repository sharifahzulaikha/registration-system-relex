<!DOCTYPE html>
<html>
<head>
	<title>Register</title>
</head>
<body>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body, html {
  height: 100%;
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

* {
  box-sizing: border-box;
}

.bg-image {
  /* The image used */
  background-image: url("relex.jpg");
  
  /* Add the blur effect */
  filter: blur(8px);
  -webkit-filter: blur(8px);
  
  /* Full height */
  height: 100%; 
  
  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}

/* Position text in the middle of the page/image */
.bg-text {
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0, 0.4); /* Black w/opacity/see-through */
  color: white;
  font-weight: bold;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 2;
  width: 80%;
  padding: 20px;

}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

.button {
  display: inline-block;
  padding: 7px 25px;
  font-size: 15px;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  outline: none;
  color: #fff;
  background-color: #74bf6c;
  border: none;
  border-radius: 4px;
  box-shadow: 0 9px #999;
}

.button:hover {
  background-color: #4caf25; /* Green */
  color: white;
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
}

.button:active {
  background-color: #3e8e41;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}

</style>
</head>
<body>
 
<div class="bg-image"></div>
<div class="bg-text">
	<img src="relexlogo.jpg" width="250" height="200" title="relex logo";>
  	<h1 style="font-size:50px">Recreation Leisure and Expedition Club</h1>
	  <?php
			date_default_timezone_set("America/New_York");
			echo date("h:i:s A"). "   ";
			echo  date("d M Y") . "<br>" . "<br>";
			?>
		
	<form action="add_student.php" method="post">
<h1>Register</h1> 
  <p> 
     <label for="fullname" class="uname" data-icon="u" > Name: </label>
     <input id="fullname" name= "username" required="required" type="text" placeholder="Full Name"/>
  </p>
  <p> 
     <label for="password" class="youpasswd" data-icon="p"> Matric ID: </label>
     <input id="password" name="matricno" required="required" type="text" placeholder="Matric ID" /> 
  </p>
	<p> 
     <label for="phone" class="youpasswd" data-icon="+6"> Phone No: </label>
     <input id="phone" name="phonenumber" required="required" type="text" placeholder="Phone No" /> 
  </p>
	<p> 
     <label for="email" class="email" data-icon="e" > Email: </label>
     <input id="email" name="email" required="required" type="text" placeholder="Email"/>
  </p>

  <p> 
     <label class="youpasswd" > Program:</label>
     <input type="checkbox" name="program" value="DIP" /> DIP
     <input type="checkbox" name="program" value="DEG" /> DEG 
  </p>
  <p> 
     <label> Course:</label>
     <select name="course" class="codrops-top">
     <option disabled="disabled" selected="selected">Choose course</option>
     <option value="CS244">CS244</option>
     <option value="CS230">CS230</option>
     <option value="CS110">CS110</option>
     <option value="CS264">CS264</option>
     <option value="CS264">CS247</option>
     </select><br>
  </p>

  <p>
     <label> Semester:</label>
     <select name="semester" class="codrops-top">
     <option disabled="disabled" selected="selected">Choose semester</option>
     <option value="Part 1">Part 1</option>
     <option value="Part 2">Part 2</option>
     <option value="Part 3">Part 3</option>
     <option value="Part 4">Part 4</option>
			<option value="Part 5">Part 5</option>
			<option value="Part 6">Part 6</option>
			<option value="Part 7">Part 7</option>
     </select>
  </p>
		<p> 
     <label class="youpasswd" > Gender:</label>
     <input type="checkbox" name="gender" value="Male" /> Male
     <input type="checkbox" name="gender" value="Female" /> Female 
  </p><br>

  <input type="hidden" name="status" value="Request"/>
<button class="button"> Register</button><br><br>
</form>

<form action="index.php" method="post">
<button class="button">Login</button>
</form>

	</div>
</body>
</html>
