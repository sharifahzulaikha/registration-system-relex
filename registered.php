<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
</head>
<body>

	<style type="text/css">
	
	#text{
		height: 20px;
		border-radius: 2px;
		padding: 10px;
		border: solid thin #aaa;
		width: 20%;
		text-align: center;
	}

	#button{
		padding: 10px;
		width: 100px;
		color: white;
		background-color: cornflowerblue;
		border: none;
		text-align: center;
	}
	</style>

<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body, html {
  height: 100%;
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

* {
  box-sizing: border-box;
}

.bg-image {
  /* The image used */
  background-image: url("relex.jpg");
  
  /* Add the blur effect */
  filter: blur(8px);
  -webkit-filter: blur(8px);
  
  /* Full height */
  height: 100%; 
  
  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}

/* Position text in the middle of the page/image */
.bg-text {
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0, 0.4); /* Black w/opacity/see-through */
  color: white;
  font-weight: bold;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 2;
  width: 80%;
  padding: 20px;
  text-align: center;
}
.button {
  display: inline-block;
  padding: 7px 25px;
  font-size: 15px;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  outline: none;
  color: #fff;
  background-color: #74bf6c;
  border: none;
  border-radius: 4px;
  box-shadow: 0 5px #999;
}

.button:hover {
  background-color: #4caf25; /* Green */
  color: white;
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
}

.button:active {
  background-color: #3e8e41;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}

</style>
</head>
<body>
        
<div class="bg-image"></div>
<div class="bg-text">
	<img src="relexlogo.jpg" width="250" height="200" title="relex logo";>
  	<h1 style="font-size:50px">Recreation Leisure and Expedition Club</h1>
	  <?php
			date_default_timezone_set("America/New_York");
			echo date("h:i:s A"). "   ";
			echo  date("d M Y") . "<br>" . "<br>";
			?>
	<p> Your registration has been requested. </p>
	
<form action="index.php" method="POST">
	<button class="button">Login</button>
</form>

</div>
</body>
</html>
