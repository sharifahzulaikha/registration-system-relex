<?php
session_start();
include 'connection.php';
$matric= $_SESSION['matric_id'];
$_SESSION['username'];

$query="SELECT * FROM student WHERE matric_id='$matric'";
$logic=mysqli_query($conn, $query);
$row=mysqli_fetch_assoc($logic);
$_SESSION['matric_id']=$matric;
?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RELEX</title>
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #00CED1;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #04AA6D;
  color: white;
}

.bg-text {
  text-align: center;
}

input[type=text], input[type=password] {
  width: 30%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

.button {
  display: inline-block;
  padding: 7px 25px;
  font-size: 15px;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  outline: none;
  color: #fff;
  background-color: #74bf6c;
  border: none;
  border-radius: 4px;
  box-shadow: 0 9px #999;
}

.button:hover {
  background-color: #4caf25; /* Green */
  color: white;
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
}

.button:active {
  background-color: #3e8e41;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}

</style>
</head>
<body>

<div class="topnav">
  <a href="home.php">Home</a>
  <a class="active" href="profile.php">Profile</a>
  <a href="activity.php">Activities</a>
  <a href="logout.php">Log Out</a>
</div>
<div class="bg-text">
	<img src="relexlogo.jpg" width="250" height="200" title="relex logo";>
</div>
<div class=bg-text>
<form  action="session_edit.php" method="POST"> 
    <h1>My Information</h1> 
    <p> 
        <label for="username" class="uname"> Name: </label>
        <input id="username" name="username" type="text" value="<?php echo $row["full_name"];?>"/>
    </p>
    <p> 
        <label for="password" class="youpasswd"> Matric No: </label>
        <input id="password" name="matric" type="text" value="<?php echo $row["matric_id"];?>"/> 
    </p>
    <p> 
        <label for="password" class="youpasswd"> Phone No: </label>
        <input id="password" name="phone" type="text" value="0<?php echo $row["phone_num"];?>"/> 
    </p>
    <p> 
        <label for="password" class="youpasswd"> Program: </label>
        <select name="program" class="codrops-top">
        <option disabled="disabled" selected="selected"><?php echo $row["program"];?></option>
        <option value="DIP">DIP</option>
        <option value="DEG">DEG</option>
      </select>
    </p>
    <p>
     <label> Course:</label>
       <select name="course" class="codrops-top">
        <option selected="selected" value="<?php echo $row["course"]?>"><?php echo $row["course"]?></option>
        <option value="CS244">CS244</option>
        <option value="CS247">CS247</option>
        <option value="CS230">CS230</option>
        <option value="CS110">CS110</option>
      </select>
      <span class="swing">      ||</span>
      <label> Semester:</label>
       <select name="semester" class="codrops-top">
        <option selected="selected" value="<?php echo $row["semester"]?>"><?php echo $row["semester"]?></option>
        <option value="Part 1">Part 1</option>
        <option value="Part 2">Part 2</option>
        <option value="Part 3">Part 3</option>
        <option value="Part 4">Part 4</option>
        <option value="Part 5">Part 5</option>
        <option value="Part 6">Part 6</option>
        <option value="Part 7">Part 7</option>
      </select>
    </p>
    <p> 
        <label> GENDER:</label>
       <select name="gender" class="codrops-top">
        <option selected="selected" value="<?php echo $row["gender"]?>"><?php echo $row["gender"]?></option>
        <option value="Male">Male</option>
        <option value="Female">Female</option>
      </select> 
    </p>
    <p> 
        <label for="password" class="youpasswd"> Email Address: </label>
        <input id="password" name="email" type="text" value="<?php echo $row["email"];?>"/> 
    </p>
    <button class="button">Update</button><br><br>
</form>
    <form action="profile.php" method="post">
    <button class="button">Cancel</button><br><br>
    </form>  

</div>
</body>
</html>