<?php
session_start();
include 'connection.php';

$_SESSION['username'];
$_SESSION['matric_id'];

?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RELEX</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #00CED1;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #04AA6D;
  color: white;
}

.bg-text {
  text-align: center;
}
.button {
  display: inline-block;
  padding: 7px 25px;
  font-size: 15px;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  outline: none;
  color: #fff;
  background-color: #74bf6c;
  border: none;
  border-radius: 4px;
  box-shadow: 0 5px #999;
}

.button:hover {
  background-color: #4caf25; /* Green */
  color: white;
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
}

.button:active {
  background-color: #3e8e41;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}

</style>
</head>
<body>

<div class="topnav">
  <a href="home.php">Home</a>
  <a href="profile.php">Profile</a>
  <a class="active" href="activity.php">Activities</a>
  <a href="logout.php">Log Out</a>
</div>

<div class="bg-text">
	<img src="relexlogo.jpg" width="250" height="200" title="relex logo";>
	  <h1>List of activity</h1>
</div>

  <p><table width="700" border="6" align="center" cellpadding="5" cellspacing="4">
    <tr>
      <td width="12">Bil</td>
      <td width="98" align="center">Activity Name</td>
      <td width="76" align="center">Location</td>
      <td width="45" align="center">Date</td>
      <td width="28" align="center">Time</td>
      <td width="73" align="center">Remaining Join</td>
      <td width="40" align="center">Action</td>
    </tr>
    <?php 
    $q="SELECT * FROM activity";
    $i=0;
    $check=mysqli_query($conn, $q);
    while($row=mysqli_fetch_assoc($check))
    { 
    $i++;
    ?>
    <tr>
    	<td height="38" align="center"><?php echo $i;?></td>
    <td align="center"><?php echo $row["act_name"];?></td>
    <td align="center"><?php echo $row["act_location"];?></td>
    <td align="center"><?php echo $row["act_date"];?></td>
    <td align="center"><?php echo $row["act_time"];?></td>
    <td align="center"><?php echo $row["act_std_qty"];?></td>
    <td align="center"><a href="session.php?act_id=<?php echo $row["act_id"];?>"><button class="button" value="REQUESTED">JOIN</button></a></td>
    </tr>
    <?php
    }
    ?>
    	</table></p>
    </body>
</html>