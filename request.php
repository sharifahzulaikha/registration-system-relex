<?php 
session_start();

	include("connection.php");
  include("function.php");

if(isset($_GET["full_name"]))
{
    $user_name=$_GET['full_name'];
    $accepted='Accepted';
    $qr="UPDATE student SET stud_status='$accepted' WHERE full_name='$user_name'";
    $log=mysqli_query($conn, $qr);
    
    if($log)
    {    
      echo '<script type="text/javascript">alert("Request Accepted")</script>';
      echo "<script type='text/javascript'>alert;window.location.href='request.php'</script>";
    }
    header("Location: display_table.php");
		die;
}

if(isset($_GET['other_name']))
{
    $user_name=$_GET['other_name'];
    $rejected='Rejected';
    $qr="UPDATE student SET stud_status='$rejected' WHERE full_name='$user_name'";
    $log=mysqli_query($conn, $qr);
    
    if($log)
    {
        echo '<script type="text/javascript">alert("Request Rejected")</script>';
        echo "<script type='text/javascript'>alert;window.location.href='request.php'</script>";

    }
}
?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Members Request</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #00CED1;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #04AA6D;
  color: white;
}

.bg-text {
  text-align: center;
}

.button {
  display: inline-block;
  padding: 7px 25px;
  font-size: 15px;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  outline: none;
  color: #fff;
  background-color: #74bf6c;
  border: none;
  border-radius: 4px;
  box-shadow: 0 5px #999;
}

.button:hover {
  background-color: #4caf25; /* Green */
  color: white;
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
}

.button:active {
  background-color: #3e8e41;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}

</style>
</head>
<body>

<div class="topnav">
  <a href="adminindex.php">Home</a>
  <a class="active" href="display_table.php">Members</a>
  <a href="admin_activity.php">Activities</a>
  <a href="logout.php">Log Out</a>
</div>

<h2>Members Request List</h2>

<p><table width="500" border="6" align="center" cellpadding="5" cellspacing="4">
	<tr>
	  <td width="10">No</td>
  	<td width="94" align="center">Name</td>
	  <td width="115" align="center">Matric ID</td>
    <td width="133" align="center">Action</td>    
	</tr>
	<?php 
  	$q="SELECT * FROM student WHERE stud_status='Request'";
  	$i=0;
  	$check=mysqli_query($conn, $q);
  	while($row=mysqli_fetch_assoc($check))
  	{ 
  		$i++;
	?>
	<tr>
		<td height="38" align="center"><?php echo $i;?></td>
  	<td align="center"><?php echo $row["full_name"];?></td>
  	<td align="center"><?php echo $row["matric_id"];?></td>
  	<td align="center"><br><a href="request.php?full_name=<?php echo $row["full_name"];?>"><button>APPROVE</button></a><br><br>
       <a href="request.php?other_name=<?php echo $row["full_name"];?>"><button>REJECT</button></a><br></td>
	</tr>
	<?php
	}
	?>
  </table></p>
  <form action="display_table.php" method="post">
    <button class="button">Back
    </button>
  </form>
