<?php 
session_start();

	include("connection.php");
  include("function.php");

$_SESSION['matric_id'];
$_SESSION['username'];

if(isset($_GET["matric_id"]))
{
    $user_id=$_GET['matric_id'];
    $accepted='Accepted';
    $qr="UPDATE activity_req SET act_status='$accepted' WHERE matric_id='$user_id'";
    $log=mysqli_query($conn, $qr);
    
    if($log)
    {
        echo '<script type="text/javascript">alert("Request Accepted")</script>';
        echo "<script type='text/javascript'>alert;window.location.href='request_activity.php'</script>";

    }
}


if(isset($_GET['other_id']))
{
    $user_id=$_GET['other_id'];
    $rejected='Rejected';
    $qr="UPDATE activity_req SET act_status='$rejected' WHERE matric_id='$user_id'";
    $log=mysqli_query($conn, $qr);
    
    if($log)
    {
        echo '<script type="text/javascript">alert("Request Rejected")</script>';
        echo "<script type='text/javascript'>alert;window.location.href='request_activity.php'</script>";

    }
}

?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RELEX</title>
<style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 16px;
}

tr:nth-child(even) {
  background-color: #f2f2f2;
}

</style>
</head>
<body>
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #00CED1;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #04AA6D;
  color: white;
}

.bg-text {
  text-align: center;
}

.button {
  display: inline-block;
  padding: 7px 25px;
  font-size: 15px;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  outline: none;
  color: #fff;
  background-color: #74bf6c;
  border: none;
  border-radius: 4px;
  box-shadow: 0 9px #999;
}}

.button:hover {
  background-color: #4caf25; /* Green */
  color: white;
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
}

.button:active {
  background-color: #3e8e41;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}

</style>

<div class="topnav">
  <a href="adminindex.php">Home</a>
  <a href="display_table.php">Members</a>
  <a class="active" href="admin_activity.php">Activities</a>
  <a href="logout.php">Log Out</a>
</div>


<h2>Request List</h2>

<p><table width="500" border="6" align="center" cellpadding="5" cellspacing="4">
	<tr>
	  <td width="10">No</td>
  	  <td width="94" align="center">Name</td>
	  <td width="115" align="center">Matric ID</td>
	  <td width="52" align="center">Activity Name</td>
      <td width="52" align="center">Date</td>
      <td width="133" align="center">Action</td>    
	</tr>
	<?php 
  	$q="SELECT * FROM activity_req WHERE act_status='Request'";
  	$i=0;
  	$check=mysqli_query($conn, $q);
  	while($row=mysqli_fetch_assoc($check))
  	{ 
  		$i++;
	?>
	<tr>
		<td height="38" align="center"><?php echo $i;?></td>
  	<td align="center"><?php echo $row["req_name"];?></td>
  	<td align="center"><?php echo $row["matric_id"];?></td>
  	<td align="center"><?php echo $row["act_name"];?></td>
    <td align="center"><?php echo $row["act_date"];?></td>
  	<td align="center"><br><a href="request_activity.php?matric_id=<?php echo $row["matric_id"];?>"><button>APPROVE</button></a><br><br>
       <a href="request_activity.php?other_id=<?php echo $row["matric_id"];?>"><button>REJECT</button></a><br></td>
	</tr>
	<?php
	}
	?>
  </table></p>
  <form action="admin_activity.php" method="post">
    <button class="button">Back
    </button>
  </form>

</body>
</html>
