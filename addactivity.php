<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RELEX</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"><style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #00CED1;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #04AA6D;
  color: white;
}

.bg-text {
  text-align: center;
}

.button {
  display: inline-block;
  padding: 7px 25px;
  font-size: 15px;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  outline: none;
  color: #fff;
  background-color: #74bf6c;
  border: none;
  border-radius: 4px;
  box-shadow: 0 5px #999;
}

.button:hover {
  background-color: #4caf25; /* Green */
  color: white;
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
}

.button:active {
  background-color: #3e8e41;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}

</style>
</head>
<body>

<div class="topnav">
  <a href="adminindex.php">Home</a>
  <a href="display_table.php">Members</a>
  <a class="active" href="admin_activity.php">Activities</a>
  <a href="logout.php">Log Out</a>
</div>

<div class="bg-text">
	<img src="relexlogo.jpg" width="250" height="200" title="relex logo";>
	  <h2>Add Activity</h2>
</div>

</head>
<body>
    <div class="container">
     <form  action="add_actdb.php" method="POST"> 
        <div class="mb-3">
          <label for="proname" class="form-label">Programme Name</label>
          <input type="text" class="form-control" name="proname" id="progname" placeholder="Programme Name">
        </div>           
        <div class="mb-3">
          <label for="dateprog" class="form-label">Date</label>
          <input type="date" class="form-control" name="dateprog" id="progname">
        </div>           
        <div class="mb-3">
          <label for="timeprog" class="form-label">Time</label>
          <input type="time" class="form-control" name="timeprog" id="progtime">
        </div>           
        <div class="mb-3">
          <label for="location" class="form-label">Location</label>
          <input type="text" class="form-control" name="location" id="location" placeholder="Location">
        </div>           
        <div class="mb-3">
          <label for="quantity" class="form-label">Total students</label>
          <input type="text" class="form-control" name="qty" id="qtyprog" placeholder="Total students">
        </div>           
           <button class="button"> Add </button><br><br>
    </form>
    <form action="admin_activity.php" method="post">
    <button class="button">Back</button><br><br>
    </form>  
</div>
</body>
</html>