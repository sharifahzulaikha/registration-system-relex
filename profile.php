<?php
session_start();

include("connection.php");
include("function.php");

?>


<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RELEX</title>
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #00CED1;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #04AA6D;
  color: white;
}

.bg-text {
  text-align: center;
}
</style>
</head>
<body>

<div class="topnav">
  <a href="home.php">Home</a>
  <a class="active" href="profile.php">Profile</a>
  <a href="activity.php">Activities</a>
  <a href="logout.php">Log Out</a>
</div>

<div class="bg-text">
	<img src="relexlogo.jpg" width="250" height="200" title="relex logo";>
</div>

</head>
<body>
</body>
</html>
<!DOCTYPE html>
<html>
<head>

<style>
* {
  box-sizing: border-box;
}

.column {
  float: left;
  width: 33.33%;
  padding: 5px;
}

/* Clearfix (clear floats) */
.row::after {
  content: "";
  clear: both;
  display: table;
}

.button {
  display: inline-block;
  padding: 7px 25px;
  font-size: 15px;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  outline: none;
  color: #fff;
  background-color: #74bf6c;
  border: none;
  border-radius: 4px;
  box-shadow: 0 5px #999;
}

.button:hover {
  background-color: #4caf25; /* Green */
  color: white;
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
}

.button:active {
  background-color: #3e8e41;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}

</style>
</head>
<body>
<?php
    $matric=$_SESSION['matric_id'];
    $qr="SELECT * FROM student WHERE matric_id='$matric'";
    $log=mysqli_query($conn, $qr);
    $row=mysqli_fetch_assoc($log);
?>
<div class="bg-text">
<h1>My Information</h1> 
        <p align="justify" class="bg-text"> 
            <label> Name: <?php echo $row["full_name"]?> </label>
        </p>
        <p align="justify" class="bg-text"> 
            <label> Matric ID: <?php echo $row["matric_id"]?> </label>
        </p>
        <p align="justify" class="bg-text"> 
            <label> Phone No: <?php echo $row["phone_num"]?> </label>
        </p>
        <p align="justify" class="bg-text"> 
            <label> Course: <?php echo $row["course"]?> </label>
        </p>
        <p align="justify" class="bg-text"> 
            <label> Program: <?php echo $row["program"]?> </label>
        </p>
        <p align="justify" class="bg-text"> 
            <label> Semester: <?php echo $row["semester"]?> </label>
        </p>
        <p align="justify" class="bg-text"> 
            <label> Gender: <?php echo $row["gender"]?> </label>
        </p>
        <p align="justify" class="bg-text"> 
            <label> Email Address: <?php echo $row["email"]?> </label>
        </p>
<form action="editinfo.php" method="post">
    <button class="button"> Edit</button><br><br>
</form>
</div>
<?php 
    $_SESSION['matric_id']=$matric;
?>

</body>
</html>