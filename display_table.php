<?php
    include 'connection.php';
    echo "<table border = '1'>
    <tr><th>No</th>
    <th>Full Name</th>
    <th>Matrix ID</th>";
    echo "<th>Phone Number</th>
    <th>Program</th>
    <th>Course</th>";
    echo "<th>Semester</th>
    <th>Gender</th>
    <th>E-mail</th></tr>";
    $result = mysqli_query($conn, "Select * from student");
    while ($row = mysqli_fetch_row($result)){
        echo "<tr>";
        foreach ($row as $cell)
            echo "<td>$cell</td>";
        echo "</tr>";    
    }
    
    mysqli_free_result($result);
    mysqli_close($conn);

?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RELEX Members</title>
<style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 16px;
}

tr:nth-child(even) {
  background-color: #f2f2f2;
}

.registerbtn {
  background-color: #04AA6D;
  color: white;
  padding: 16px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
  text-align: center;
}
</style>
</head>
<body>
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #00CED1;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #04AA6D;
  color: white;
}

.bg-text {
  text-align: center;
}

.button {
  display: inline-block;
  padding: 7px 25px;
  font-size: 15px;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  outline: none;
  color: #fff;
  background-color: #74bf6c;
  border: none;
  border-radius: 4px;
  box-shadow: 0 5px #999;
}

.button:hover {
  background-color: #4caf25; /* Green */
  color: white;
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
}

.button:active {
  background-color: #3e8e41;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}
</style>

<div class="topnav">
  <a href="adminindex.php">Home</a>
  <a class="active" href="display_table.php">Members</a>
  <a href="admin_activity.php">Activities</a>
  <a href="logout.php">Log Out</a>
</div>

<h2>Members of RELEX Club</h2>

<table>
</table><br>
</form>

<form action="request.php" method="post">
<button class="button">Request List</button>
</form>

</body>
</html>
