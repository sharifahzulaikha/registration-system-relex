<?php 

session_start();

	include("connection.php");
	include("function.php");


	if($_SERVER['REQUEST_METHOD'] == "POST")
	{
		//something was posted
		If (isset($_POST['ad_username'], $_POST['ad_matricno']))
		{
			$adusername = $_POST['ad_username'];
		    $admatricno = $_POST['ad_matricno'];
		}

		if(!empty($adusername) && !empty($admatricno) && !is_numeric($adusername))
		{

			//read from database
			$query = "select * from admin where ad_username = '$adusername' limit 1";
			$result = mysqli_query($conn, $query);

			if($result)
			{
				if($result && mysqli_num_rows($result) > 0)
				{

					$user_data = mysqli_fetch_assoc($result);
					
					if($user_data['ad_matricno'] === $admatricno)
					{

						$_SESSION['admin_id'] = $user_data['admin_id'];
						header("Location: adminindex.php");
						die;
					}
				}
			}
			
			echo "wrong username or password!";
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Admin Login</title>
</head>
<body>

	<style type="text/css">
	
	#text{
		height: 20px;
		border-radius: 2px;
		padding: 10px;
		border: solid thin #aaa;
		width: 20%;
		text-align: center;
	}

	</style>

<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body, html {
  height: 100%;
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

* {
  box-sizing: border-box;
}

.bg-image {
  /* The image used */
  background-image: url("relex.jpg");
  
  /* Add the blur effect */
  filter: blur(8px);
  -webkit-filter: blur(8px);
  
  /* Full height */
  height: 100%; 
  
  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}

/* Position text in the middle of the page/image */
.bg-text {
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0, 0.4); /* Black w/opacity/see-through */
  color: white;
  font-weight: bold;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 2;
  width: 80%;
  padding: 20px;
  text-align: center;
}

.button {
  display: inline-block;
  padding: 7px 25px;
  font-size: 15px;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  outline: none;
  color: #fff;
  background-color: #74bf6c;
  border: none;
  border-radius: 4px;
  box-shadow: 0 5px #999;
}

.button:hover {
  background-color: #4caf25; /* Green */
  color: white;
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
}

.button:active {
  background-color: #3e8e41;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}

</style>
</head>
<body>
        
<div class="bg-image"></div>
<div class="bg-text">
	<img src="relexlogo.jpg" width="250" height="200" title="relex logo";>
  	<h1 style="font-size:50px">Recreation Leisure and Expedition Club</h1>
      <h2>Admin Login</h2>
	  <?php
			date_default_timezone_set("America/New_York");
			echo date("h:i:s A"). "   ";
			echo  date("d M Y") . "<br>" . "<br>";
			?>
	<form method = "post">
            USERNAME: <input id="text" type = "text" name = "ad_username" placeholder="Name" required><br><br>
            PASSWORD: <input id="text" type = "password" name = "ad_matricno" placeholder="Matric id" required><br><br>
            <button class="button">Login</button><br><br>
	</form>
    <form action="index.php" method="POST">
			<button class="button"> Student Login</button><br>
    </form>

</div>
</body>
</html>